#!/usr/bin/env python3

# send the routing message specified in the argument
# to the channel /apollo/routing request

import sys
import time
import argparse
import json

from cyber.python.cyber_py3 import cyber
from cyber.python.cyber_py3 import cyber_time
from modules.routing.proto.routing_pb2 import RoutingRequest


class CyberShutdown(Exception):
    pass


def request_park_routing(routing_file):
    ego_routing = {}
    with open(routing_file, 'r+') as f:
        ego_routing = json.load(f)
    cyber.init()

    if not cyber.ok():
        print('cyber error')
        sys.exit(1)

    
    node = cyber.Node("mock_park_routing_requester")
    sequence_num = 0

    routing_request = RoutingRequest()

    routing_request.header.timestamp_sec = cyber_time.Time.now().to_sec()
    routing_request.header.module_name = 'routing_request'
    routing_request.header.sequence_num = sequence_num
    sequence_num = sequence_num + 1

    waypoint_num = len(ego_routing["waypoints"])
    for i in range(waypoint_num):
        point1 = routing_request.waypoint.add()
        point1.pose.x = ego_routing["waypoints"][i]["x"]
        point1.pose.y = ego_routing["waypoints"][i]["y"]

    
    routing_request.parking_info.parking_space_id = ego_routing["parking_info"]["parking_space_id"]
    parking_point = routing_request.parking_info.parking_point
    parking_point.x = ego_routing["parking_info"]["parking_point"]["x"]
    parking_point.y = ego_routing["parking_info"]["parking_point"]["y"]


    writer = node.create_writer('/apollo/routing_request', RoutingRequest)
    time.sleep(2.0)
    writer.write(routing_request)
    time.sleep(2.0)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f', '--filename',
        help='The config file path of the ego\'s routing',
        type=str
    )
    args = parser.parse_args()

    request_park_routing(args.filename)

    time.sleep(30.0)


if __name__ == '__main__':
    main()
